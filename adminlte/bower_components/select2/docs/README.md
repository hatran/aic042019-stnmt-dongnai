Effective beginning September 10, 2018, the Select2 documentation repository is now available at [`select2/docs`](https://github.com/select2/docs).
